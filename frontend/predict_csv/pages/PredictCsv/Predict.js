import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import { bindRoutineCreators } from 'actions'
import { InfoBox, PageContent } from 'components'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { RunPredict } from 'comps/predict_csv/components'

class Predict extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stepsEnabled: true,
      initialStep: 0,
      steps: [
        {
          element: '.predict-page',
          intro: 'Data prediction can be made on this page.',
        },
        {
          element: '.RunPredict',
          intro: 'Here dropped files will be predicted.',
        },
        {
          element: '.predict-results',
          intro: 'Results of prediction are listed here.',
        },
        {
          element: '.predict-download',
          intro: 'Predicted files can be downloaded here.',
        },
      ],
    }
  }


  render() {
    const { stepsEnabled, steps, initialStep } = this.state
    return (
      <PageContent className='predict-page'>
         <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Predict CSV</title>
        </Helmet>
        <h1>Predict!</h1>
        <RunPredict />
      </PageContent>)
  }
}


export default compose(
)(Predict)
