import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { loadRecordPrediction } from 'comps/predict_csv/actions'
import PredictApi from 'comps/predict_csv/api'
import { selectRecordPrediction } from 'comps/predict_csv/reducers/recordPrediction'


export const KEY = 'recordPrediction'

export const maybeLoadRecordPredictionSaga = function* (id) {
  const { byId, isLoading } = yield select(selectRecordPrediction)
  const isLoaded = !!byId[id]
  if (!(isLoaded || isLoading)) {
    yield put(loadRecordPrediction.trigger(id))
  }
}

export const loadRecordPredictionSaga = createRoutineSaga(
  loadRecordPrediction,
  function *successGenerator({ payload: id }) {
    id = yield call(PredictApi.loadRecordPrediction, id)
    yield put(loadRecordPrediction.success({ id }))
  }
)

export default () => [
  takeEvery(loadRecordPrediction.MAYBE_TRIGGER, maybeLoadRecordPredictionSaga),
  takeLatest(loadRecordPrediction.TRIGGER, loadRecordPredictionSaga),
]
