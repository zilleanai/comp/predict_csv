import { loadRecordPrediction } from 'comps/predict_csv/actions'


export const KEY = 'prediction'

const initialState = {
  isLoading: false,
  ids: [],
  byId: {},
  error: null,
}

export default function(state = initialState, action) {
  const { type, payload } = action
  const { id } = payload || {}
  const { ids, byId } = state

  switch (type) {
    case loadRecordPrediction.REQUEST:
      return { ...state,
        isLoading: true,
      }

    case loadRecordPrediction.SUCCESS:
      if (!ids.includes(id.id)) {
        ids.push(id.id)
      }
      byId[id.id] = id
      return { ...state,
        ids,
        byId,
      }

    case loadRecordPrediction.FULFILL:
      return { ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectRecordPrediction = (state) => state[KEY]
export const selectRecordPredictionById = (state, id) => selectRecordPrediction(state).byId[id]
