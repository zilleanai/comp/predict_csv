import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function predict(uri) {
  return v1(`/predict_csv${uri}`)
}

export default class Predict {

static loadRecordPrediction({id}) {
  return get(predict(`/download/${id}`))
}

}
