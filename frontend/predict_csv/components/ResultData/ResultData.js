import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { List } from 'immutable';
import './result-data.scss'
import { CheckboxList } from 'components'
import { v1 } from 'api'

import { loadRecordPrediction } from 'comps/predict_csv/actions'
import { selectRecordPredictionById } from 'comps/predict_csv/reducers/recordPrediction'

import Plot from 'react-plotly.js';

class ResultData extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      features: ['price'],
      features2: ['action'],
      time_: List(),
      data: { 'time': List(), },
    }
  }

  componentWillMount() {
    const { id, loadRecordPrediction } = this.props
    loadRecordPrediction.maybeTrigger({ id })
  }

  componentDidMount() {
    const { isLoaded } = this.props
    if (!isLoaded) {
      this.timeout = setTimeout(() => {
        const { id, loadRecordPrediction } = this.props
        loadRecordPrediction.maybeTrigger({ id })
      }, 2000);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { loadRecordPrediction, id, prediction } = nextProps
    loadRecordPrediction.maybeTrigger({ id })

    if (nextProps.isLoaded) {
      var time_ = this.state.time_
      var data = this.state.data || {}
      time_ = prediction.data.time
      for (var key in prediction.data) {
        data[key] = prediction.data[key] || []
      }
      for (var key in prediction.predict) {
        data[key] = prediction.predict[key] || []
      }

      this.setState({
        time_: time_,
        data: data,
      })
    }
  }

  handleFeatures = (features) => {
    this.setState({ features: features });
  }

  handleFeatures2 = (features) => {
    this.setState({ features2: features });
  }

  plotData(features, features2, data) {
    var result = []
    if (features2 && features2.length > 0 && data['time']) {
      result = features2.map((feature, i) => {
        if (data[feature] && data['time']) {
          var d = {
            x: data['time'],
            y: data[feature],
            mode: "lines+points",
            yaxis: 'y2',
            type: 'scatter',
            mode: 'lines+points',
            name: feature
          }
        } else {
          d = {}
        }
        return d
      })
    }
    if (features && features.length > 0 && data['time']) {
      let feature_data = features.map((feature, i) => {
        if (data[feature] && data['time']) {
          var d = {
            x: data['time'],
            y: data[feature],
            type: 'scatter',
            mode: 'lines+points',
            name: feature
          }
        } else {
          d = {}
        }
        return d
      })
      return [...feature_data, ...result]
    }
    return [...result]
  }

  render() {
    const { id, isLoaded, error, loadRecordPrediction } = this.props
    if (!isLoaded || error) {
      return (
        <button onClick={()=>{loadRecordPrediction.maybeTrigger({ id })}}>Reload</button>
      )
    }
    return (
      <div className='result-data'>
        <CheckboxList items={Object.keys(this.state.data).filter(e => e !== 'index' && e !== 'time')}
          onSelectItems={this.handleFeatures} defaultValue={['price']}></CheckboxList>
        <CheckboxList items={Object.keys(this.state.data).filter(e => e !== 'index' && e !== 'time')}
          onSelectItems={this.handleFeatures2} defaultValue={['action']}></CheckboxList>
        <Plot
          data={this.plotData(this.state.features, this.state.features2, this.state.data)}
          layout={{
            title: id, yaxis: { title: '' },
            yaxis2: {
              title: '',
              overlaying: 'y',
              side: 'right'
            }
          }}
        />
        <a href={v1(`/predict/download/${id}`)} >Download</a>
      </div>
    )
  }
}

const withReducer = injectReducer(require('comps/predict_csv/reducers/recordPrediction'))
const withSaga = injectSagas(require('comps/predict_csv/sagas/recordPrediction'))

const withConnect = connect(
  (state, props) => {
    const id = props.id
    const prediction = selectRecordPredictionById(state, id)
    return {
      id,
      prediction,
      isLoaded: !!prediction,
    }
  },
  (dispatch) => bindRoutineCreators({ loadRecordPrediction }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ResultData)
