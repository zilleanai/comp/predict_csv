import React from 'react'
import { compose } from 'redux'
import './run-predict.scss'
import { v1 } from 'api'
import { FilePond, File, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import { storage } from 'comps/project'
import { ResultData } from 'comps/predict_csv/components'

registerPlugin(FilePondPluginImagePreview);


function predict(uri) {
  return v1(`/predict${uri}`)
}

class RunPredict extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // Set initial files
      files: [],
      results: [],
      resultids: []
    };
  }

  handleInit() {
  }

  render() {
    const { results, resultids } = this.state
    const server = {
      url: predict(`/predict/${storage.getProject()}`),
      timeout: 160000,
      process: {
        onload: (res) => {
          const res_obj = JSON.parse(res)
          const result = {
            id: res_obj['id'],
          }
          const results = this.state.results
          const resultids = this.state.resultids
          results.push(result)
          resultids.push(result.id)
          this.setState({ results: results, resultids: resultids })
          return res;
        }
      }
    }

    return (
      <div className="RunPredict">

        {/* Pass FilePond properties as attributes */}
        <FilePond ref={ref => this.pond = ref}
          allowMultiple={true}
          maxFiles={100}
          server={server}
          oninit={() => this.handleInit()}
          onupdatefiles={(fileItems) => {
            // Set current file objects to this.state

            this.setState({
              files: fileItems.map(fileItem => fileItem.file)
            });
          }}>

          {/* Update current files  */}


        </FilePond>
        <label>results:</label>
        <ul className="predict-results">
          {results.map((result, i) => {
            return (
              <li key={i} >
                <ResultData id={result['id']} />
              </li>
            )
          })}
        </ul>
        <a className='predict-download' href={predict(`/files/${storage.getProject()}?ids=${resultids.join(',')}`)} download={`${storage.getProject()}_predicts.zip`}><button>Download!</button></a>
      </div>
    );
  }
}

export default compose(
)(RunPredict)
