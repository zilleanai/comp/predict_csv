import os
import json
import pickle
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
import zipfile
import uuid

import pandas as pd
from werkzeug.utils import secure_filename


class PredictCsvController(Controller):

    @route('/download/<string:id>')
    def download(self, id):
        redis = AppConfig.SESSION_REDIS
        cached = redis.get(id)
        if not cached:
            return abort(404)
        data_io = BytesIO(cached)
        data_io.seek(0)
        df = pd.read_csv(data_io)
        df = df.fillna(0)
        cached = redis.get(''+id+'_orig')
        if not cached:
            return abort(404)
        data_io = BytesIO(cached)
        data_io.seek(0)
        df2 = pd.read_csv(data_io)
        df2 = df2.fillna(0)
        return jsonify(id=id, data=df2.reset_index().to_dict(orient='list'), predict=df.reset_index().to_dict(orient='list'))