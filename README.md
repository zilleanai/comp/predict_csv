# predict_csv

Component for prediction of csv data.

## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/predict_csv
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.predict',
    'bundles.predict_csv',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.predict.routes'),
    include('bundles.predict_csv.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  PredictCsv,
} from 'comps/predict_csv/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  PredictCsv: 'PredictCsv',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.PredictCsv,
    path: '/predict_csv',
    component: PredictCsv,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.PredictCsv} />
    ...
</div>
```
